# Pogretra v1.0
Bruce Robertson bruce.g.robertson@gmail.com

POlytonic GReek TRAining data and classifiers derived from historical texts, a product of the Open Greek and Latin project.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[Open Database License](https://opendatacommons.org/licenses/odbl/1-0/)
