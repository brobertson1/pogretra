
J. B. COTELERII JUDICIUM 
DE 
POSTERIORE EPISTOLA S. CLEMENTIS. 
EXSTAT de hac Epistola judicium magni Critici Eu- 
sebii, Hist. Eccles. iii 38, hisce verbis expressum; ἰστέον 
δ’ ὡς καὶ δευτέρα τις εἶναι λέγεται τοῦ Κλήμεντος Ἐπιστολή· οὐ 
μὴν ἐθ’ ὁμοίως τῇ προτέρᾳ καὶ ταύτην γνώριμον ἐπιστάμεθα, 
ὅτι μηδὲ τοὺς ἀρχαίους αὐτῇ κεχρημένους ἴσμεν. Quae a Ru- 
fino, uti solet, indiligenter redduntur sic : " Dicitur tamen 
" esse et alia Clementis Epistola, cujus nos notitiam non 
" accepimus." Nec magis accurate Hieronymus, Libro de 
Viris Illustribus : " Fertur et secunda esse ex ejus nomine 
" Epistola, quae a Veteribus reprobatur." [ 16, tom. ii 
p. 843 ed. Vallars.] Et Photius, Μυριοβίβλου Cod. cxii, 
cxiii, ἡ δὲ λεγομένη δευτέρα πρὸς τοὺς αὐτοὺς ὡς νόθος ἀποδο- 
κιμάζεται. Neque enim dicit Eusebius, secundam Clementis 
Epistolam "notham esse, et ab Antiquis reprobatam ;" 
dicit solimimodo "ignoratam fuisse, in citationibus a Vete- 
" ribus praetermissam, in dubium vocatam :" unde eodem 
loci et alibi priorem nuncupat ὁμολογουμένην, ἀνωμολογη- 
μένην παρὰ πᾶσιν. Itaque rectius in Nicephoro, iii 18, 
habetur; καὶ ἄλλη δὲ αὐτοῦ φέρεται Ἐπιστολή, τῆς προτέρας 
κατὰ πολὺ ἀποδέουσα· περὶ ἧς ὁ αὐτός φησιν Εὐσεβίος, μὴ ἐν 
ἐπιστήμῃ ταύτης τοὺς ἀρχαίους εἶναι. Maximus in Prologo 
ad Dionysii Opera, agens de Eusebio, καὶ μὴν οὔτε Πανταίνου 
τοὺς πόνους ἀνέγραψεν, οὔτε τοῦ Ῥωμαίου Κλήμεντος, πλὴν δύο 
καὶ μόνων Ἐπιστολῶν. Et vero quemadmodum Caesariensi 
F f 2 
