
BOOK I, XVII. 25-XVIII. 2 
Once more then, it is the decision of your own will 
which compelled you, that is, moral purpose com- 
pelled moral purpose. For if God had so constructed 
that part of His own being which He has taken 
from Himself and bestowed upon us, that it could 
be subjected to hindrance or constraint either from 
Himself or from some other. He were no longer 
God, nor would He be caring for us as He ought. 
This is what I find," says the diviner, "in the sacri- 
fice. These are the signs vouchsafed you. If you 
will, you are free ; if you will, you will not have to, 
blame anyone, or complain against anyone; every- 
will be in accordance with what is not merely 
your own will, but at the same time the will of God." 
This is the prophecy for the sake of which I go to 
this diviner — in other words, the philosopher, — not 
admiring him because of his interpretation, but 
rather the interpretation which he gives. 
CHAPTER XVIII 
That we ought not to be angry with the erring 
IF what the philosophers1 say is true, that in all 
men thought and action start from a single source, 
namely feeling — as in the case of assent the feeling 
that a thing is so, and in the case of dissent the 
feeling that it is not so, yes, and, by Zeus, in the case of 
suspended judgement the feeling that it is uncertain, 
so also in the case of impulse towards a thing, the 
feeling that it is expedient for me and that it is impos- 
to judge one thing expedient and yet desire 
another, and again, to judge one thing fitting, and 
yet be impelled to another — if all this be true, why 
VOL. I. F 121 
