
847b ΑΡΙΣΤΟΤΕΛΟΥΣ 
δεῖ δὲ λαβεῖν ἅμα καὶ τὰ συμβαίνοντα περὶ τὴν γέ- 
36 νεσιν αὐτῆς, τά τε μὴ πλανῶντα καὶ τὰ δοκοῦντ’ εἶναι πα- 
ράλογα. ἔστι μὲν γὰρ ἢ χάλαζα κρύσταλλος, πήγνυται δὲ 
τὸ ὕδωρ τοῦ χειμῶνος· αἱ δὲ χάλαζαι γίγνονται ἔαρος μὲν 
348a καὶ μετοπώρου μάλιστα, εἶτα δὲ καὶ τῆς ὀπώρας, χειμῶνος 
δ’ ὀλιγάκις, καὶ ὅταν ἧττον ᾖ φῦχος. καὶ ὅλως δὲ γίγνον- 
ται χάλαζαι μὲν ἐν τοῖς εὐδιεινοτέροις τόποις, αἱ δὲ χιόνες 
ἐν τοῖς ψυχροτέροις. ἄτοπον δὲ καὶ τὸ πήγνυσθαι ὕδωρ ἐν 
5 τῷ ἄνω τόπῳ οὔτε γὰρ παγῆναι δυνατὸν πρὶν γενέσθαι 
ὕδωρ, οὔτε τὸ ὕδωρ οὐδένα χρόνον οἶόν τε μένειν μετέωρον ὄν. 
ἀλλὰ μὴν οὐδ’ ὥσπερ αἱ ψακάδες ἄνω μὲν ὀχοῦνται διὰ 
μικρότητα, ἐνδιατρίψασαι δ’ ἐπὶ τοῦ ἀέρος, ὥσπερ καὶ ἐπὶ 
τοῦ ὕδατος γῆ καὶ χρυσὸς διὰ μικρομέρειαν πολλάκις ἐπι-
10 πλέουσιν, οὕτως ἐπὶ τοῦ ἀέρος τὸ ὕδωρ, συνελθόντων δὲ πολ- 
λῶν μικρῶν μεγάλαι καταφέρονται φακάδες· τοῦτο γὰρ οὐκ 
ἐνδέχεται γενέσθαι ἐπὶ τῆς χαλάζης· οὐ γὰρ συμφύεται τὰ 
πεπηγότα ὡσπερ τὰ ὑγρά. δῆλον οὖν ὅτι ἄνω τοσοῦτον ὕδωρ 
ἔμεινεν· οὐ γὰρ ἂν ἐπάγη τοσοῦτον. τοῖς μὲν οὖν δοκεῖ τοῦ πά- 
15 θους αἴτιον εἶναι τούτου καὶ τῆς γενέσεως, ὅταν ἀπωσθῇ τὸ 
νέφος εἰς τὸν ἄνω τόπον μᾶλλον ὄντα ψυχρὸν διὰ τὸ λή- 
γειν ἐκεῖ τὰς ἀπὸ τῆς γῆς τῶν ἀκτίνων ἀνακλάσεις, ἐλθὸν 
δ’ ἐκεῖ πήγνυσθαι τὸ ὕδωρ· διὸ καὶ θέρους μᾶλλον καὶ 
ἐν ταῖς ἀλεειναῖς χώραις γίγνεσθαι τὰς χαλάζας, ὅτι ἐπὶ 
2Ο πλέον τὸ θερμὸν ἀνωθεῖ ἀπὸ τῆς γῆς τὰς νεφέλας συμ- 
βαίνει δ’ ἐν τοῖς σφόδρα ὑφηλοῖς ἤκιστα γίγνεσθαι χάλα- 
ζαν· καίτοι ἔδει, ὥσπερ καὶ τὴν χιόνα ὁρῶμεν ἐπὶ τοῖς 
ὑφηλοῖς μάλιστα γιγνομένην. ἔτι δὲ πολλάκις ὦπται νέφη 
36 κρύσταλος Ecorr. m.1 𝔐  Η Ν ‖ 37 ἀέρος J1 Ecorr. m.i ‖ 348 a 1 μετωπόρου 
J ‖ δὲ om. Ε 𝔅 ‖ 2 ἦττον om. Ν1 𝔅 ‖ 3 ἐν — χιόνες marg. E1 ‖ αἰ δὲ χιόνες 
χίονες δ’ Ald. ‖ 6 οὐδένα] οὐχ ἕνα 2B ‖ τε ex τε ται corr. Ei ‖ ὂν] ὢν N1: om. 
Ε F ‖ 7 ψεκάδες J1 Ecorr. m.1 cet. ‖ ὀχλοῦνται 𝔐: (ὀ- in ras. E1) ‖ 8 καὶ om. 
Η Ν ‖  9 χρυσὸς] χρυσός ὃ 𝔅  ‖ 11 ψεκάδες J1 Ecorr. m.1 cet. ‖ γὰρ] δὲ Em.1 in 
ras. 𝔅  ‖ 12 συμφύσεται 𝔅 ‖ 13 ὅτι] ὄτ F || 14 ἔμεινεν ex γέγονεν corr. Ν1 ‖ 
ἐπάγει 𝔄1 ‖ τοῦ] τοῦτο τοῦ Ald. ‖ 15 τοῦτο Ε 𝔅 ‖ ἀπωσθῇ ex ἀποσθῆ (ut 
videtur) corr.  𝔐1: ex ἀπωθῆ (ut videtur) corr. 𝔄1: ἀποστῆ Ν ‖ 17 ἐλθών 
𝔄1 ‖ 20 συμβαίνει] συμβαι Ε1 ‖ 21 + 23 ὑψιλοῖς J ‖ 21 χαλάζας E 𝔅  ‖ 22 καἰ 
om. Ε 
