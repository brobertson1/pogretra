
BOOK VII. 168-169 
the nonce promised to send help and protection, 
declaring that they could not suffer Hellas to perish, 
— for if she should fall, of a surety the very next 
day would see them also enslaved, — but they must 
render aid to the best of their power. Thus they 
gave a specious answer; but when the time came for 
sending help, their minds were changed; they 
manned sixty ships, and did with much ado put out 
to sea and make the coast of the Peloponnese; but 
there they anchored off Pylos and Taenarus in the 
Lacedaemonian territory, waiting like the others to 
see which way the war should incline; they had no 
hope that the Greeks would prevail, but thought 
that the Persian would win a great victory and be 
lord of all Hellas. What they did, therefore, was 
done of set purpose, that they might be able to say 
to the Persian, "Ο king, we whose power is as great 
as any, and who could have furnished as many ships 
as any state save Athens, — we, when the Greeks 
essayed to gain our aid in this war, would not resist 
you nor do aught displeasing to you." This plea 
they hoped would win them some advantage more 
than ordinary; and so, methinks, it would have 
been. But they were ready with an excuse which 
they could make to the Greeks, and in the end they 
made it ; when the Greeks blamed them for sending 
no help, they said that they had manned sixty 
triremes, but by stress of the etesian winds they 
could not round Malea ; thus it was (they said) that 
they could not arrive at Salamis : it was no craven 
spirit that made them late for the sea-fight. 
169. With such a plea they put the Greeks off. 
But the Cretans, when the Greeks appointed to 
deal with them strove to gain their aid, did as I will 
483 
II 2 
